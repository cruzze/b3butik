/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP (1000) [Manufacturer]
      ,[Model]
      ,[ModelYear]
      ,[Language]
      ,[OS]
      ,[OSVersion]
  FROM [aruba].[vDeviceTypeDim]

SET IDENTITY_INSERT [dim].[DeviceTypeDim] ON;  

INSERT INTO [dim].[DeviceTypeDim]
           ([DeviceTypeDim_SK],[Manufacturer]
           ,[Model]
           ,[ModelYear]
           ,[Language]
           ,[OS]
           ,[OSVersion])
     VALUES
           (-1,'N/A'
           ,'N/A'
           ,'N/A'
           ,'N/A'
           ,'N/A'
           ,'N/A')


		   
SET IDENTITY_INSERT [dim].[DeviceTypeDim] OFF;  


SET IDENTITY_INSERT [dim].[CustomerDim] ON;  


INSERT INTO [dim].[CustomerDim]
           ([CustomerDim_SK],[CustomerExternalId]
           ,[KeyType]
           ,[Hashed]
           ,[Randomized]
           ,[CustomerInfo]
           ,[Email]
           ,[Username])
     VALUES
           (-2, 'Ej inloggad randomized'
           ,'HashedMacAdress'
           ,1
           ,1
           ,'Ej inloggad och randomized'
           ,NULL
           ,'N/A')

		   
INSERT INTO [dim].[CustomerDim]
           ([CustomerDim_SK],[CustomerExternalId]
           ,[KeyType]
           ,[Hashed]
           ,[Randomized]
           ,[CustomerInfo]
           ,[Email]
           ,[Username])
     VALUES
           (-1, 'Ej inloggad'
           ,'HashedMacAdress'
           ,1
           ,1
           ,'Ej inloggad ej randomized'
           ,NULL
           ,'N/A')
GO


SET IDENTITY_INSERT [dim].[CustomerDim] OFF; 


SET IDENTITY_INSERT [dim].[StoreDim] ON;

INSERT INTO [dim].[StoreDim]
           ([StoreDim_SK],[StoreExternalId]
           ,[Name]
           ,[StreetAdress]
           ,[Zipcode]
           ,[City]
           ,[Region]
           ,[Country]
           ,[Longitude]
           ,[Latitude]
           ,[RetailerDim_SK]
           ,[CorporateExternaldentity]
           ,[Email]
           ,[PhoneNumber]
           ,[LongitudeList]
           ,[LatitudeList])
     VALUES
           (-1,'-1'
           ,'N/A'
           ,'N/A'
           ,'N/A'
           ,'N/A'
           ,'N/A'
           ,'N/A'
           ,0.0
           ,0.0
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL)

SET IDENTITY_INSERT [dim].[StoreDim] OFF;




SET IDENTITY_INSERT [dim].[FloorDim] ON;

INSERT INTO [dim].[FloorDim]
           ([FloorDim_SK],[StoreExternalId]
           ,[FloorExternalId]
           ,[Floor]
           ,[FloorName]
           ,[StoreDim_SK]
           ,[StartDate]
           ,[EndDate])
     VALUES
           (-1, '-1'
           ,'-1'
           ,0
           ,'N/A'
           ,-1
           ,'0001-01-01'
           ,'9999-12-31'
)

GO

SET IDENTITY_INSERT [dim].[FloorDim] OFF;

SET IDENTITY_INSERT [dim].[DepartmentDim] ON;


INSERT INTO [dim].[DepartmentDim]
           ([DepartmentDim_SK],[StoreExternalId]
           ,[DepartmentExternalId]
           ,[Department]
           ,[LongitudeList]
           ,[LatitudeList]
           ,[FloorDim_SK]
           ,[StartDate]
           ,[EndDate])
     VALUES
           (-1,'-1'
           ,'N/A'
           ,'N/A'
           ,NULL
           ,NULL
           ,-1
           ,'0001-01-01'
           ,'9999-12-31'
)

SET IDENTITY_INSERT [dim].[DepartmentDim] OFF;




SET IDENTITY_INSERT [dim].[ZoneDim] ON;

INSERT INTO [dim].[ZoneDim]
           ([ZoneDim_SK],[StoreExternalId]
           ,[ZoneExternalId]
           ,[Zone]
           ,[LongitudeList]
           ,[LatitudeList]
           ,[DepartmentDim_SK]
           ,[StartDate]
           ,[EndDate])
     VALUES
           (-1,'-1'
           ,'-1'
           ,'N/A'
           ,NULL
           ,NULL
           ,-1
           ,'0001-01-01'
           ,'9999-12-31'
)
GO
SET IDENTITY_INSERT [dim].[ZoneDim] OFF;


SET IDENTITY_INSERT [dim].[ControllerDeviceDim] ON;


INSERT INTO [dim].[ControllerDeviceDim]
           ([ControllerDeviceDim_SK],[IPAddres]
           ,[MACAddress]
           ,[Name]
           ,[Longitude]
           ,[Latitude]
           ,[StartDate]
           ,[EndDate]
           ,[RSSILimitStorePresence])
     VALUES
           (-1,'-1'
           ,'-1'
           ,'N/A'
           ,0.0
           ,0.0
           ,'0001-01-01'
           ,'9999-12-31'
           ,0
)
GO

SET IDENTITY_INSERT [dim].[ControllerDeviceDim] OFF;
