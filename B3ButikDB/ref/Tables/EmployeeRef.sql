﻿CREATE TABLE [ref].[EmployeeRef] (
    [StoreExternalid]    VARCHAR (255) NOT NULL,
    [CustomerExternalId] VARCHAR (50)  NOT NULL,
    [StartDate]          DATETIME2 (0) NOT NULL,
    [EndDate]            DATETIME2 (0) NOT NULL,
    CONSTRAINT [PK_EMPLOYEEREF] PRIMARY KEY CLUSTERED ([StoreExternalid] ASC, [CustomerExternalId] ASC)
);

