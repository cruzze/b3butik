﻿CREATE TABLE [ref].[HolidayRef] (
    [Country]       VARCHAR (50) NOT NULL,
    [Date]          DATE         NOT NULL,
    [Name]          VARCHAR (50) NOT NULL,
    [PublicHoliday] BIT          NOT NULL,
    CONSTRAINT [PK_HOLIDAYREF] PRIMARY KEY CLUSTERED ([Country] ASC, [Date] ASC)
);

