﻿CREATE TABLE [ref].[WeatherRef] (
    [WeatherCollected] DATETIME2 (7)  NOT NULL,
    [Temp]             INT            NULL,
    [Wind]             INT            NULL,
    [Weather]          VARCHAR (4000) NULL,
    [Location]         VARCHAR (255)  NOT NULL,
    PRIMARY KEY CLUSTERED ([WeatherCollected] ASC, [Location] ASC)
);

