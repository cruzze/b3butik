﻿
CREATE procedure ref.loadHolidagSwe AS
BEGIN

DECLARE @StartDate DATETIME = '01/01/2018' --Starting value of Date Range
DECLARE @EndDate DATETIME = '01/01/'+cast(cast(left(convert(varchar, getdate(), 121),4) as int)+3 as varchar) --End Value of Date Range

INSERT INTO [ref].[HolidayRef]
SELECT 'Sverige', Påskdagen, 'Påskdagen', 1
FROM dim.DateDim 
CROSS APPLY (SELECT DateDim.Year AS y) _y
CROSS APPLY (SELECT y / 100 as c, y - 19 * (y / 19) AS n) _nc
CROSS APPLY (SELECT (c - 17) / 25 AS k) _k
CROSS APPLY (SELECT c - c/4 - (c - k)/3 + 19 *n + 15 AS i1) _i1
CROSS APPLY (SELECT i1 - 30 * (i1 / 30) AS i2) _i2
CROSS APPLY (SELECT i2 - (i2 / 28) * (1 - (i2 / 28) * (29 / (i2 + 1)) *
		     ((21 - n) / 11)) AS i) _i
CROSS APPLY (SELECT y + y / 4 + i + 2 - c + c / 4 AS j1) _j1
CROSS APPLY (SELECT j1 - 7*(j1 / 7) AS j) _j
CROSS APPLY (SELECT i - j AS el) _el
CROSS APPLY (SELECT 3 + (el + 40) / 44 AS m) _m
CROSS APPLY (SELECT el + 28 - 31*(m / 4) AS d) _d
CROSS APPLY (SELECT DATEFROMPARTS(y, m, d) AS Påskdagen) _Easter
WHERE DateDim.Date = Påskdagen
and Date between @StartDate and @EndDate

INSERT INTO [ref].[HolidayRef]
select 'Sverige', DATEADD(dd, -3, [Date]), 'Skärtorsdagen', 0  from [ref].[HolidayRef]
where [Name] = 'Påskdagen'
and Date between @StartDate and @EndDate

INSERT INTO [ref].[HolidayRef]
select 'Sverige', DATEADD(dd, -2, [Date]), 'Långfredagen', 1  from [ref].[HolidayRef]
where [Name] = 'Påskdagen'
and Date between @StartDate and @EndDate

INSERT INTO [ref].[HolidayRef]
select 'Sverige', DATEADD(dd, -1, [Date]), 'Påskafton', 1  from [ref].[HolidayRef]
where [Name] = 'Påskdagen'
and Date between @StartDate and @EndDate

INSERT INTO [ref].[HolidayRef]
select 'Sverige', DATEADD(dd, 1, [Date]), 'Annandag påsk', 1  from [ref].[HolidayRef]
where [Name] = 'Påskdagen'
and Date between @StartDate and @EndDate

INSERT INTO [ref].[HolidayRef]
select 'Sverige', DATEADD(dd, 39, [Date]), 'Kristi himmelsfärdsdag', 1  from [ref].[HolidayRef]
where [Name] = 'Påskdagen'
and Date between @StartDate and @EndDate

INSERT INTO [ref].[HolidayRef]
select 'Sverige', DATEADD(dd, 49, [Date]), 'Pingstdagen', 0  from [ref].[HolidayRef]
where [Name] = 'Påskdagen'
and Date between @StartDate and @EndDate

DECLARE @CurrentDate AS DATETIME = @StartDate
DECLARE @M TINYINT 
DECLARE @D TINYINT 
DECLARE @DW TINYINT 

WHILE @CurrentDate < @EndDate
BEGIN
	SET @M = DATEPART(MM, @CurrentDate)
	SET @D = DATEPART(DD, @CurrentDate)
	SET @DW = CASE WHEN DATEPART(DW, @CurrentDate) = 1 THEN 7 ELSE DATEPART(DW, @CurrentDate) - 1 END -- Måndag = 1 söndag = 7

	IF (@M = 1 AND @D = 1) INSERT INTO [ref].[HolidayRef]
		select 'Sverige', @CurrentDate, 'Nyårsdagen', 1 
	IF (@M = 1 AND @D = 6) INSERT INTO [ref].[HolidayRef]
		select 'Sverige', @CurrentDate, 'Trettondag jul', 1 
	IF (@M = 4 AND @D = 30) INSERT INTO [ref].[HolidayRef]
		select 'Sverige', @CurrentDate, 'Valborgsmässoafton', 0 
	IF (@M = 5 AND @D = 1) INSERT INTO [ref].[HolidayRef]
		select 'Sverige', @CurrentDate, '1 maj', 1 
	IF (@M = 6 AND @D = 6) INSERT INTO [ref].[HolidayRef]
		select 'Sverige', @CurrentDate, 'Nationaldagen', 1 
	IF (@M = 6 AND @DW = 5 and @D BETWEEN 19 AND 25 ) INSERT INTO [ref].[HolidayRef]
		select 'Sverige', @CurrentDate, 'Midsommarafton', 0 
	IF (@M = 6 AND @DW = 6 and @D BETWEEN 20 AND 26 ) INSERT INTO [ref].[HolidayRef]
		select 'Sverige', @CurrentDate, 'Midsommardagen', 1 
	IF (@M = 10 AND @DW = 5 and @D between 30 and 31 ) OR (@M = 11 AND @DW = 5 and @D BETWEEN 01 AND 5 ) INSERT INTO [ref].[HolidayRef]
		select 'Sverige', @CurrentDate, 'Alla helgons afton', 0 
	IF (@M = 10 AND @DW = 6 and @D = 31 ) OR (@M = 11 AND @DW = 6 and @D BETWEEN 01 AND 6 ) INSERT INTO [ref].[HolidayRef]
		select 'Sverige', @CurrentDate, 'Alla helgons dag', 1 
	IF (@M = 12 AND @D = 24) INSERT INTO [ref].[HolidayRef]
		select 'Sverige', @CurrentDate, 'Julafton', 0
	IF (@M = 12 AND @D = 25) INSERT INTO [ref].[HolidayRef]
		select 'Sverige', @CurrentDate, 'Juldagen', 1 
	IF (@M = 12 AND @D = 26) INSERT INTO [ref].[HolidayRef]
		select 'Sverige', @CurrentDate, 'Annandag jul', 1 
	IF (@M = 12 AND @D = 31) INSERT INTO [ref].[HolidayRef]
		select 'Sverige', @CurrentDate, 'Nyårsafton', 0

	SET @CurrentDate = DATEADD(DD, 1, @CurrentDate) 
END --while

END -- procedure