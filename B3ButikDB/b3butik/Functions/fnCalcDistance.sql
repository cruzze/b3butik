﻿
CREATE FUNCTION b3butik.[fnCalcDistance](@lat1 FLOAT, @lat2 FLOAT, @lon1 FLOAT, @lon2 FLOAT)
RETURNS FLOAT 
AS
BEGIN
	DECLARE @distance FLOAT
	IF (@lat1 != @lat2) AND (@lon1 != @lon2)
		BEGIN
	    SET @distance = ACOS(NULLIF(SIN(PI()*@lat1/180.0)*SIN(PI()*@lat2/180.0)+COS(PI()*@lat1/180.0)*COS(PI()*@lat2/180.0)*COS(PI()*@lon2/180.0-PI()*@lon1/180.0), 1))*6371000
		END
	ELSE
		BEGIN
		SET @distance = 0
		END
	RETURN @distance
END