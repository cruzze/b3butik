﻿

CREATE procedure [dim].[loadTime] AS
BEGIN

--Load time data for every second of a day
DECLARE @Time DATETIME

SET @TIME = CONVERT(VARCHAR,'12:00:00 AM',108)

--TRUNCATE TABLE [dim].[TimeDim]

WHILE @TIME <= '11:59:59 PM'
 BEGIN
 INSERT INTO [dim].[TimeDim]
           ([Time]
           ,[Hour]
           ,[HourText]
           ,[Minute]
           ,[MinuteText]
           ,[Second]
           ,[SecondText])
 SELECT CONVERT(VARCHAR,@TIME,108) [Time]
 ,DATEPART(HOUR,@TIME) [Hour]
 , RIGHT(REPLICATE('0', 2) + CAST(DATEPART(HOUR,@TIME) AS VARCHAR(2)), 2) AS [HourText]
 , DATEPART(MINUTE,@Time) [Minute]
 , RIGHT(REPLICATE('0', 2) + CAST(DATEPART(HOUR,@TIME) AS VARCHAR(2)), 2) AS [MinuteText]
 , DATEPART(SECOND,@Time) [Second]
 , RIGHT(REPLICATE('0', 2) + CAST(DATEPART(HOUR,@TIME) AS VARCHAR(2)), 2) AS [SecondText]

 SELECT @TIME = DATEADD(second,1,@Time)
 END

END