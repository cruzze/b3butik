﻿


CREATE procedure [dim].[loadDateDim] AS
BEGIN

/********************************************************************************************/
--Specify Start Date and End date here
--Value of Start Date Must be Less than Your End Date 

--print cast(left(convert(varchar, getdate(), 121),4) as int)+2
--truncate table dim.DateDim

DECLARE @StartDate DATETIME = '01/01/2018' --Starting value of Date Range
DECLARE @EndDate DATETIME = '01/01/'+cast(cast(left(convert(varchar, getdate(), 121),4) as int)+3 as varchar) --End Value of Date Range

--Temporary Variables To Hold the Values During Processing of Each Date of Year
DECLARE @CurrentDate AS DATETIME = @StartDate

/********************************************************************************************/
--Proceed only if Start Date(Current date ) is less than End date you specified above

WHILE @CurrentDate < @EndDate
BEGIN

/* Populate Your Dimension Table with values*/
INSERT INTO dim.DateDim
           ([DateDim_SK]
           ,[Date]
           ,[Year]
           ,[HalfYear]
           ,[HalfYearText]
           ,[Quarter]
           ,[QuarterText]
           ,[Month]
           ,[MonthName]
           ,[MonthNameShort]
           ,[YearMonth]
           ,[WeekISO]
           ,[YearWeekISO]
           ,[YearWeekISOText]
           ,[DayOfWeek]
           ,[DayName]
           ,[DayOfYear]
           ,[DayOfMonth]
           ,[IsWeekday])
SELECT
CAST(CONVERT (char(8),@CurrentDate,112) AS INT) as [DateDim_SK]
,@CurrentDate AS [Date]--Date
,DATEPART(yy, @CurrentDate)  AS [Year]
,CASE WHEN DATEPART(mm, @CurrentDate) < 7 THEN DATEPART(yy, @CurrentDate) * 10 + 1 
ELSE DATEPART(yy, @CurrentDate) * 10 + 2 END AS [HalfYear] 
,CASE WHEN DATEPART(mm, @CurrentDate) < 7 THEN 'Halvår 1 ' +  CAST(DATEPART(yy, @CurrentDate) AS CHAR(4))
ELSE 'Halvår 2 ' +  CAST(DATEPART(yy, @CurrentDate) AS CHAR(4)) END  [HalfYearText]
,DATEPART(qq, @CurrentDate) AS [Quarter]
,'Kv ' + CAST(DATEPART(qq, @CurrentDate) AS CHAR(2)) +  CAST(DATEPART(yy, @CurrentDate) AS CHAR(4)) AS [QuarterText]
,DATEPART(mm, @CurrentDate) [Month]
,CASE DATENAME(MM, @CurrentDate) 
WHEN 'January' THEN 'januari'
WHEN 'February' THEN 'februari'
WHEN 'March' THEN 'mars'
WHEN 'April' THEN 'april'
WHEN 'May' THEN 'maj'
WHEN 'June' THEN 'juni'
WHEN 'July' THEN 'juli'
WHEN 'August' THEN 'augusti'
WHEN 'September' THEN 'september'
WHEN 'October' THEN 'oktober'
WHEN 'November' THEN 'november'
WHEN 'December' THEN 'december'
END AS [MonthName]

,CASE DATENAME(MM, @CurrentDate) 
WHEN 'January' THEN 'jan'
WHEN 'February' THEN 'feb'
WHEN 'March' THEN 'mar'
WHEN 'April' THEN 'apr'
WHEN 'May' THEN 'maj'
WHEN 'June' THEN 'jun'
WHEN 'July' THEN 'juli'
WHEN 'August' THEN 'aug'
WHEN 'September' THEN 'sep'
WHEN 'October' THEN 'okt'
WHEN 'November' THEN 'nov'
WHEN 'December' THEN 'dec'

END AS [MonthNameShort]
, DATEPART(yy, @CurrentDate) * 100 + DATEPART(mm, @CurrentDate)  [YearMonth]

, DATEPART(isoww, @CurrentDate)[WeekISO]

, DATEPART(yy, @CurrentDate) * 100 + DATEPART(isoww, @CurrentDate) [YearWeekISO]

,  'v' + CAST(DATEPART(isoww, @CurrentDate) AS CHAR(2)) + ' ' + CAST(DATEPART(yy, @CurrentDate) AS CHAR(4)) [YearWeekISOText]

,CASE DATEPART(DW, @CurrentDate)
WHEN 1 THEN 7
WHEN 2 THEN 1
WHEN 3 THEN 2
WHEN 4 THEN 3
WHEN 5 THEN 4
WHEN 6 THEN 5
WHEN 7 THEN 6
END 
AS [DayOfWeek]

,CASE DATENAME(DW, @CurrentDate) 
when 'Monday' THEN 'måndag'
when 'Tuesday' THEN 'tisdag'
when 'Wednesday' THEN 'onsdag'
when 'Thursday' THEN 'torsdag'
when 'Friday' THEN 'fredag'
when 'Saturday' THEN 'lördag'
when 'Sunday' THEN 'söndag'
else ''
END AS [DayName]--DayName

,DATEPART(DY, @CurrentDate) AS [DayOfYear]--DayOfYear

,DATEPART(DD, @CurrentDate) AS [DayOfMonth]-- DayOfMonth

,CASE DATEPART(DW, @CurrentDate)
WHEN 1 THEN 0
WHEN 2 THEN 1
WHEN 3 THEN 1
WHEN 4 THEN 1
WHEN 5 THEN 1
WHEN 6 THEN 1
WHEN 7 THEN 0
END AS IsWeekday


SET @CurrentDate = DATEADD(DD, 1, @CurrentDate)
END -- END WHILE

END --END Procedure