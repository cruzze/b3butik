﻿CREATE TABLE [dim].[DepartmentDim] (
    [DepartmentDim_SK]     INT           IDENTITY (1, 1) NOT NULL,
    [StoreExternalId]      VARCHAR (255) NULL,
    [DepartmentExternalId] VARCHAR (255) NULL,
    [Department]           VARCHAR (255) NOT NULL,
    [LongitudeList]        VARCHAR (MAX) NULL,
    [LatitudeList]         VARCHAR (MAX) NULL,
    [FloorDim_SK]          INT           NOT NULL,
    [StartDate]            DATETIME2 (0) NULL,
    [EndDate]              DATETIME2 (0) NULL,
    [Inserttime_DT]        DATETIME2 (7) CONSTRAINT [DF_DepartmentDim_Timestamp] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_DEPARTMENT_DIM] PRIMARY KEY CLUSTERED ([DepartmentDim_SK] ASC),
    CONSTRAINT [fk_DepartmentDim_FloorDim_1] FOREIGN KEY ([FloorDim_SK]) REFERENCES [dim].[FloorDim] ([FloorDim_SK])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_DepartmentDim]
    ON [dim].[DepartmentDim]([StoreExternalId] ASC, [DepartmentExternalId] ASC, [StartDate] ASC);

