﻿CREATE TABLE [dim].[RetailerDim] (
    [RetailerDim_SK]     INT           IDENTITY (1, 1) NOT NULL,
    [RetailerExternalId] VARCHAR (255) NOT NULL,
    [Name]               VARCHAR (255) NOT NULL,
    [StreetAddress]      VARCHAR (255) NOT NULL,
    [ZipCode]            VARCHAR (20)  NOT NULL,
    [City]               VARCHAR (50)  NOT NULL,
    [PhoneNumber]        VARCHAR (50)  NULL,
    [Email]              VARCHAR (255) NULL,
    [Inserttime_DT]      DATETIME2 (7) CONSTRAINT [DF_RetailerDim_Timestamp] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_RETAILERDIM] PRIMARY KEY CLUSTERED ([RetailerDim_SK] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_RetailerDim]
    ON [dim].[RetailerDim]([RetailerExternalId] ASC);

