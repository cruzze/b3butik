﻿CREATE TABLE [dim].[StoreDim] (
    [StoreDim_SK]              INT              IDENTITY (1, 1) NOT NULL,
    [StoreExternalId]          VARCHAR (255)    NULL,
    [Name]                     VARCHAR (100)    NOT NULL,
    [StreetAdress]             VARCHAR (100)    NOT NULL,
    [Zipcode]                  VARCHAR (20)     NOT NULL,
    [City]                     VARCHAR (50)     NOT NULL,
    [Region]                   VARCHAR (50)     NOT NULL,
    [Country]                  VARCHAR (50)     NOT NULL,
    [Longitude]                NUMERIC (18, 15) NOT NULL,
    [Latitude]                 NUMERIC (18, 15) NOT NULL,
    [RetailerDim_SK]           INT              NULL,
    [CorporateExternaldentity] VARCHAR (20)     NULL,
    [Email]                    VARCHAR (255)    NULL,
    [PhoneNumber]              VARCHAR (50)     NULL,
    [LongitudeList]            VARCHAR (MAX)    NULL,
    [LatitudeList]             VARCHAR (MAX)    NULL,
    [Inserttime_DT]            DATETIME2 (7)    CONSTRAINT [DF_StoreDim_Timestamp] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_STOREDIM] PRIMARY KEY CLUSTERED ([StoreDim_SK] ASC),
    CONSTRAINT [fk_StoreDim_RetailerDim_1] FOREIGN KEY ([RetailerDim_SK]) REFERENCES [dim].[RetailerDim] ([RetailerDim_SK])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_STOREDIM]
    ON [dim].[StoreDim]([StoreExternalId] ASC);

