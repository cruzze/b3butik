﻿CREATE TABLE [dim].[SourceSystemDim] (
    [SourceSystemDim_SK] INT           IDENTITY (1, 1) NOT NULL,
    [Provider_name]      VARCHAR (255) NOT NULL,
    [Manufacturer]       VARCHAR (255) NOT NULL,
    [XYZUnit]            VARCHAR (20)  NULL,
    CONSTRAINT [PK_SOURCESYSTEMDIM] PRIMARY KEY CLUSTERED ([SourceSystemDim_SK] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_SourceSystemDim]
    ON [dim].[SourceSystemDim]([Provider_name] ASC);

