﻿CREATE TABLE [dim].[ZoneDim] (
    [ZoneDim_SK]       INT           IDENTITY (1, 1) NOT NULL,
    [StoreExternalId]  VARCHAR (255) NULL,
    [ZoneExternalId]   VARCHAR (255) NULL,
    [Zone]             VARCHAR (255) NOT NULL,
    [LongitudeList]    VARCHAR (MAX) NULL,
    [LatitudeList]     VARCHAR (MAX) NULL,
    [DepartmentDim_SK] INT           NOT NULL,
    [StartDate]        DATETIME2 (0) NOT NULL,
    [EndDate]          DATETIME2 (0) NOT NULL,
    CONSTRAINT [PK_ZONEDIM] PRIMARY KEY CLUSTERED ([ZoneDim_SK] ASC),
    CONSTRAINT [fk_ZoneDim_DepartmentDim_1] FOREIGN KEY ([DepartmentDim_SK]) REFERENCES [dim].[DepartmentDim] ([DepartmentDim_SK])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_ZoneDim]
    ON [dim].[ZoneDim]([StoreExternalId] ASC, [ZoneExternalId] ASC);

