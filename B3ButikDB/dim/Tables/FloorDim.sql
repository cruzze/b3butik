﻿CREATE TABLE [dim].[FloorDim] (
    [FloorDim_SK]     INT           IDENTITY (1, 1) NOT NULL,
    [StoreExternalId] VARCHAR (255) NULL,
    [FloorExternalId] VARCHAR (255) NULL,
    [Floor]           INT           NOT NULL,
    [FloorName]       VARCHAR (255) NOT NULL,
    [StoreDim_SK]     INT           NOT NULL,
    [StartDate]       DATETIME2 (0) NOT NULL,
    [EndDate]         DATETIME2 (0) NOT NULL,
    [Inserttime_DT]   DATETIME2 (7) CONSTRAINT [DF_FloorDim_Timestamp] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_FLOORDIM] PRIMARY KEY CLUSTERED ([FloorDim_SK] ASC),
    CONSTRAINT [fk_FloorDim_StoreDim_1] FOREIGN KEY ([StoreDim_SK]) REFERENCES [dim].[StoreDim] ([StoreDim_SK])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_FLOORDIM]
    ON [dim].[FloorDim]([StoreExternalId] ASC, [FloorExternalId] ASC, [StartDate] ASC);

