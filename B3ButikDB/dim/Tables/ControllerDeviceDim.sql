﻿CREATE TABLE [dim].[ControllerDeviceDim] (
    [ControllerDeviceDim_SK] INT              IDENTITY (1, 1) NOT NULL,
    [IPAddres]               VARCHAR (50)     NULL,
    [MACAddress]             VARCHAR (20)     NULL,
    [Name]                   VARCHAR (255)    NULL,
    [Longitude]              NUMERIC (18, 15) NULL,
    [Latitude]               NUMERIC (18, 15) NULL,
    [StartDate]              DATETIME2 (0)    NULL,
    [EndDate]                DATETIME2 (0)    NULL,
    [RSSILimitStorePresence] INT              NULL,
    [Inserttime_DT]          DATETIME2 (7)    CONSTRAINT [DF_ControllerDeviceDim_Timestamp] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_CONTROLLERDEVICEDIM] PRIMARY KEY CLUSTERED ([ControllerDeviceDim_SK] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_ControllerDeviceDim]
    ON [dim].[ControllerDeviceDim]([IPAddres] ASC, [MACAddress] ASC);

