﻿CREATE TABLE [dim].[TimeDim] (
    [TimeDim_SK] INT      IDENTITY (1, 1) NOT NULL,
    [Time]       CHAR (8) NOT NULL,
    [Hour]       SMALLINT NOT NULL,
    [HourText]   CHAR (2) NOT NULL,
    [Minute]     SMALLINT NOT NULL,
    [MinuteText] CHAR (2) NOT NULL,
    [Second]     SMALLINT NOT NULL,
    [SecondText] CHAR (2) NOT NULL,
    CONSTRAINT [PK_TIMEDIM] PRIMARY KEY CLUSTERED ([TimeDim_SK] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_TIMEDIM]
    ON [dim].[TimeDim]([Time] ASC);

