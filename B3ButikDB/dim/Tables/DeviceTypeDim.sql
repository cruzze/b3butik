﻿CREATE TABLE [dim].[DeviceTypeDim] (
    [DeviceTypeDim_SK] INT           IDENTITY (1, 1) NOT NULL,
    [Manufacturer]     VARCHAR (255) NULL,
    [Model]            VARCHAR (255) NULL,
    [ModelYear]        VARCHAR (255) NULL,
    [Language]         VARCHAR (255) NULL,
    [OS]               VARCHAR (255) NULL,
    [OSVersion]        VARCHAR (255) NULL,
    [Inserttime_DT]    DATETIME2 (7) CONSTRAINT [DF_DeviceTypeDim_Timestamp] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_DEVICETYPEDIM] PRIMARY KEY CLUSTERED ([DeviceTypeDim_SK] ASC)
);

