﻿CREATE TABLE [dim].[DateDim] (
    [DateDim_SK]      INT           NOT NULL,
    [Date]            DATE          NOT NULL,
    [Year]            SMALLINT      NOT NULL,
    [HalfYear]        INT           NOT NULL,
    [HalfYearText]    VARCHAR (50)  NOT NULL,
    [Quarter]         INT           NOT NULL,
    [QuarterText]     VARCHAR (50)  NOT NULL,
    [Month]           SMALLINT      NOT NULL,
    [MonthName]       VARCHAR (50)  NOT NULL,
    [MonthNameShort]  VARCHAR (6)   NOT NULL,
    [YearMonth]       INT           NOT NULL,
    [WeekISO]         SMALLINT      NOT NULL,
    [YearWeekISO]     INT           NOT NULL,
    [YearWeekISOText] VARCHAR (50)  NOT NULL,
    [DayOfWeek]       SMALLINT      NOT NULL,
    [DayName]         VARCHAR (50)  NOT NULL,
    [DayOfYear]       SMALLINT      NOT NULL,
    [DayOfMonth]      SMALLINT      NOT NULL,
    [IsWeekday]       BIT           NOT NULL,
    [Inserttime_DT]   DATETIME2 (7) CONSTRAINT [DF_DateDim_Timestamp] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_DATEDIM] PRIMARY KEY CLUSTERED ([DateDim_SK] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_DATEDIM]
    ON [dim].[DateDim]([Date] ASC);

