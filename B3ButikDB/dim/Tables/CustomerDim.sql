﻿CREATE TABLE [dim].[CustomerDim] (
    [CustomerDim_SK]     INT           IDENTITY (1, 1) NOT NULL,
    [CustomerExternalId] VARCHAR (50)  NOT NULL,
    [KeyType]            VARCHAR (20)  NOT NULL,
    [Hashed]             BIT           DEFAULT ((0)) NOT NULL,
    [Randomized]         BIT           DEFAULT ((0)) NOT NULL,
    [CustomerInfo]       VARCHAR (255) NULL,
    [Email]              VARCHAR (255) NULL,
    [Username]           VARCHAR (255) NULL,
    [Inserttime_DT]      DATETIME2 (7) CONSTRAINT [DF_CustomerDim_Timestamp] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_CUSTOMERDIM] PRIMARY KEY CLUSTERED ([CustomerDim_SK] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_CustomerDim]
    ON [dim].[CustomerDim]([CustomerExternalId] ASC, [KeyType] ASC, [Username] ASC);

