﻿
CREATE view aruba.vVisitFactPrevDay AS 
WITH BESOK AS 
(
	select DISTINCT
	-- Nu har vi besökets start och sluttidpunkt i UTC
	-- Gör om dessa till LocalTime
	Hashed_sta_eth_mac
	,[Campus_id]
	,starttidUTC
	,sluttidUTC
	,(((CONVERT([datetime],starttidUTC) AT TIME ZONE 'UTC') AT TIME ZONE 'Central European Standard Time')) visitStartTime
	,(((CONVERT([datetime],sluttidUTC) AT TIME ZONE 'UTC') AT TIME ZONE 'Central European Standard Time')) visitEndTime
	,datediff(SECOND, starttidUTC,sluttidUTC) as seconds
	,[Provider_name]
	from 
	(	
		-- Leta reda på sista posten i data som är större än startidpunkt
		SELECT 
		x.Hashed_sta_eth_mac
		,x.[Campus_id]
		,x.starttidUTC
		,x.nasta_starttidUTC
		,x.[Provider_name]
		,max(a.TimestampUTC ) as sluttidUTC
		FROM 
		(
			-- Plocka ut alla där föregående post är null eller > 5 min = starttidpunkt för besök
			-- och leta reda på nästa rads startdatum
			select 
			[Hashed_sta_eth_mac]
			,[Provider_name]
			,[Campus_id]
			,[TimestampUTC] as starttidUTC
			,LEAD([TimestampUTC]) OVER (PARTITION BY [Hashed_sta_eth_mac], [Campus_id] ORDER BY [TimestampUTC] ASC) nasta_starttidUTC
			from (
				-- Räkna ut tidsdifferens till föregående post
				select  
				[Hashed_sta_eth_mac]
				, [Provider_name]
				, [TimestampUTC] 
				, [Campus_id]
				,lag([TimestampUTC]) OVER (PARTITION BY [Hashed_sta_eth_mac] ORDER BY [TimestampUTC] ASC) tidigare_TIME
				,datediff(second, lag([TimestampUTC]) OVER (PARTITION BY [Hashed_sta_eth_mac], [Campus_id] ORDER BY [TimestampUTC] ASC),[TimestampUTC] ) as seconds
				FROM [aruba].[ArubaLocationData] 
				where CAST([TimestampUTC] AS DATE) = DATEADD(day, -1 , CAST(getdate() AS DATE))
				and [Username] IS NOT NULL AND [Username] != 'NotAvailable'
			)X
			where seconds IS NULL OR seconds >= 300
		) X
		-- Här försvinner en del poster för att en besök måste bestå av minst två registreringar
		INNER JOIN [aruba].[ArubaLocationData] A
		on a.Hashed_sta_eth_mac = x.Hashed_sta_eth_mac and a.TimestampUTC > x.starttidUTC and a.TimestampUTC < nasta_starttidUTC
		GROUP BY 
		x.Hashed_sta_eth_mac
		,x.[Provider_name]
		,x.[Campus_id]
		,x.starttidUTC
		,x.nasta_starttidUTC 
	)x
),
--SELECT * FROM BESOK
DISTANCE AS 
(
-- Räkna ut distance
SELECT 
b.Hashed_sta_eth_mac
,b.[Campus_id] 
,b.starttidUTC
,b.sluttidUTC
,b.visitStartTime
,b.visitEndTime
,b.seconds
,SUM(distance) as Distance
,count(*) as SeenCount
,max(Rssi_val) as StrengthMax
FROM BESOK B
INNER JOIN 
(
	SELECT 
	x.Hashed_sta_eth_mac,
	x.[TimestampUTC],
	x.Rssi_val
	,case when x.[latitude] <>  x.lagLatitude or  x.[longitude]  <> x.lagLongitude THEN
			b3butik.fnCalcDistance( x.[latitude] , x.lagLatitude , x.[longitude] , x.lagLongitude)
			ELSE 0 END as distance
	FROM
	(
	select
	a.Hashed_sta_eth_mac,
	a.[TimestampUTC],
	a.Longitude, 
	a.Latitude,
	a.Rssi_val
	,lag(a.Longitude) OVER (PARTITION BY a.[Hashed_sta_eth_mac], a.[Campus_id] ORDER BY a.[TimestampUTC] ASC)  lagLongitude
	,lag(a.Latitude) OVER (PARTITION BY a.[Hashed_sta_eth_mac], a.[Campus_id] ORDER BY a.[TimestampUTC] ASC) lagLatitude
	from 
	[aruba].[ArubaLocationData] A
	INNER JOIN BESOK B
	ON b.Hashed_sta_eth_mac = a.Hashed_sta_eth_mac and a.[TimestampUTC] between b.starttidUTC and b.sluttidUTC
	)X
)A
--[aruba].[ArubaLocationData] A
on a.Hashed_sta_eth_mac = b.Hashed_sta_eth_mac and a.TimestampUTC BETWEEN b.visitStartTime and b.visitEndTime
GROUP BY 
b.Hashed_sta_eth_mac
,b.[Campus_id]
,b.starttidUTC
,b.sluttidUTC
,b.visitStartTime
,b.visitEndTime
,b.seconds
)
--select * from DISTANCE
--*****************************
select DISTINCT
	  v.visitStartTime as [VisitStarttime]
	  ,CASE WHEN a.[Sta_eth_mac] = 'Anonymized' 
		  THEN v.[Hashed_sta_eth_mac] 
		  ELSE SUBSTRING(a.[Sta_eth_mac], 1, 2) + ':' + SUBSTRING( a.[Sta_eth_mac], 3, 2) + ':' + SUBSTRING( a.[Sta_eth_mac], 5, 2) + ':'  + SUBSTRING( a.[Sta_eth_mac], 7, 2) + ':'  + SUBSTRING( a.[Sta_eth_mac], 9, 2) + ':' + SUBSTRING( a.[Sta_eth_mac], 11, 2) 
		  END AS  [CustomerExternalId]
      ,v.[VisitEndTime]
      ,v.seconds AS  [VisitLengthSeconds]
      ,CASE WHEN a.Username IS NOT NULL AND a.Username != 'NotAvailable' THEN 1 ELSE 0 END AS [LoggedIn]
      , v.visitStartTime AS [LoggedInStartTime]
      ,v.[VisitEndTime] AS [LoggedInEndTime]
      ,v.seconds AS [LoggedInLengthSeconds]
      ,v.[SeenCount]
      ,v.[StrengthMax]
      ,v.[Distance]
      ,[StoreDim_SK]
      ,[SourceSystemDim_SK]
      ,[CustomerDim_SK]
      ,[DateDim_SK]
      ,[TimeDim_SK]
      ,COALESCE([DeviceTypeDim_SK], -1) AS [DeviceTypeDim_SK]
from 
DISTANCE V
INNER JOIN
(
	SELECT DISTINCT 
	a.[Hashed_sta_eth_mac]
	,a.[TimestampUTC]
	,a.Provider_name
	,'Aruba' as [Manufacturer]
	,a.Rssi_val
	,a.[Device_family]
	,a.[Device_type]
	,a.Sta_eth_mac
	,a.Building_id
	,a.Username
	,CASE WHEN [Sta_eth_mac] = 'Anonymized' 
		  THEN a.[Hashed_sta_eth_mac] 
		  ELSE SUBSTRING([Sta_eth_mac], 1, 2) + ':' + SUBSTRING( [Sta_eth_mac], 3, 2) + ':' + SUBSTRING( [Sta_eth_mac], 5, 2) + ':'  + SUBSTRING( [Sta_eth_mac], 7, 2) + ':'  + SUBSTRING( [Sta_eth_mac], 9, 2) + ':' + SUBSTRING( [Sta_eth_mac], 11, 2)  
		  END AS  [CustomerExternalId]

	from 
	[aruba].[ArubaLocationData] A
	INNER JOIN BESOK B
	ON b.Hashed_sta_eth_mac = a.Hashed_sta_eth_mac and a.[TimestampUTC] = b.starttidUTC 
) A
ON A.[Hashed_sta_eth_mac] = v.Hashed_sta_eth_mac and a.TimestampUTC = v.starttidUTC
LEFT OUTER JOIN
[dim].[CustomerDim] C ON
c.CustomerExternalId = a.[CustomerExternalId] and c.[KeyType] = 'MacAdress' and c.Username = a.Username
LEFT OUTER JOIN 
[dim].[StoreDim] s ON
s.[StoreExternalId] = a.Building_id
LEFT OUTER JOIN
[dim].[TimeDim] t on
t.Time = CONVERT(VARCHAR(8), visitStartTime, 108) 
LEFT OUTER JOIN
[dim].[DateDim] d on 
d.Date = cast(v.visitStartTime as DATE)
LEFT OUTER JOIN
[dim].[SourceSystemDim] ss on
ss.[Provider_name] = coalesce(a.[Provider_name], 'IPONLY') and ss.[Manufacturer] = a.[Manufacturer]
LEFT OUTER JOIN
[dim].[DeviceTypeDim] dt on
dt.Manufacturer = a.[Device_family] and dt.Model = a.[Device_type] and 
dt.Language is NULL and dt.ModelYear IS NULL and dt.OS IS NULL and dt.OSVersion IS NULL