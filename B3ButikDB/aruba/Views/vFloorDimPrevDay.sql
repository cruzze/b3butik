﻿

/****** Script for SelectTopNRows command from SSMS  ******/
CREATE VIEW [aruba].[vFloorDimPrevDay] AS
SELECT DISTINCT
	Building_id  AS [StoreExternalId]  
    ,[Floor_id] AS [FloorExternalId]
      ,-1 AS [Floor]
      ,'N/A'  AS [FloorName]
      ,S.[StoreDim_SK]
      ,DATEADD(day, -1 ,CAST(GETDATE() AS DATE)) AS [StartDate]
      ,'9999-12-31' AS [EndDate]
  FROM [aruba].[ArubaLocationData] A
  INNER JOIN [dim].[StoreDim] S
  ON s.[StoreExternalId] =  A.Building_id
  where NOT EXISTS (SELECT [FloorExternalId] FROM [dim].[FloorDim] F where [FloorExternalId] = [Floor_id])
  AND CAST([TimestampUTC] AS DATE) <= DATEADD(day, -1 , CAST(getdate() AS DATE))