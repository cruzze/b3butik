﻿
CREATE VIEW aruba.vPositionFactPrevDay AS
SELECT  
[Positiontime]
      ,[Hashed_sta_eth_mac] AS [CustomerExternalId]
      ,x.[Longitude]
      ,x.[Latitude]
      ,x.[Rssi_val] as [Strength]
      ,[LoggedIn]
      ,NULL AS [X]
      ,NULL AS [y]
      ,NULL AS [Z]
	,case when x.[latitude] <>  x.lagLatitude or  x.[longitude]  <> x.lagLongitude AND seconds < 300  THEN
			b3butik.fnCalcDistance( x.[latitude] , x.lagLatitude , x.[longitude] , x.lagLongitude)
			ELSE 0 END as distance
	  ,[SourceSystemDim_SK]
      ,s.[StoreDim_SK]
      ,f.[FloorDim_SK]
      ,-1 [DepartmentDim_SK]
      ,-1 AS [ZoneDim_SK]
      ,[CustomerDim_SK]
      ,[DateDim_SK]
      ,[TimeDim_SK]
      ,COALESCE([DeviceTypeDim_SK],-1) AS [DeviceTypeDim_SK]
      ,COALESCE([ControllerDeviceDim_SK], -1) AS [ControllerDeviceDim_SK]
from (
SELECT [Seq]
      ,[TimestampUTC]
      ,[op]
      ,[Topic_seq]
      ,[Source_id]
      ,[Topic]
      ,[Hashed_sta_eth_mac]
      ,[Sta_location_x]
      ,[Sta_location_y]
      ,[Error_level]
      ,[Campus_id]
      ,[Building_id]
      ,[Floor_id]
      ,[Sta_eth_mac]
      ,[Geofence_ids]
      ,[Rssi_val]
      ,[Loc_algorithm]
      ,[Longitude]
      ,[Latitude]
      ,[Altitude]
      ,[Unit]
      ,[EventProcessedUtcTime]
      ,[PartitionId]
      ,[EventEnqueuedUtcTime]
      ,[Associated]
      ,[Device_category]
      ,[Device_family]
      ,[Device_type]
	  ,CASE WHEN [Username] IS NOT NULL and [Username] != 'NotAvailable' THEN [Username] ELSE 'N/A' END AS  [Username]
      ,[Sta_ip_address]
      ,[Sta_ip_af]
      ,[Hashed_sta_ip_address]
      ,[Role]
      ,[Ht]
      ,[Ap_name]
      ,[Ap_mac]
      ,[Bssid]
      ,coalesce([Randomized_mac], 0) AS [Randomized_mac]
      ,[Provider_name]
	  ,CASE WHEN Username IS NOT NULL and Username != 'NotAvailable' THEN 'MacAdress' ELSE 'HashedMacAdress' END AS  [KeyType]
	  ,CASE WHEN Username IS NOT NULL and Username != 'NotAvailable' THEN 1 ELSE 0 END AS  [LoggedIn]
	  ,(((CONVERT([datetime],TimestampUTC) AT TIME ZONE 'UTC') AT TIME ZONE 'Central European Standard Time')) [Positiontime]
	  ,lag(a.Longitude) OVER (PARTITION BY a.[Hashed_sta_eth_mac], a.[Campus_id] ORDER BY a.[TimestampUTC] ASC)  lagLongitude
	  ,lag(a.Latitude) OVER (PARTITION BY a.[Hashed_sta_eth_mac], a.[Campus_id] ORDER BY a.[TimestampUTC] ASC) lagLatitude
	  ,datediff(second, lag([TimestampUTC]) OVER (PARTITION BY [Hashed_sta_eth_mac], [Campus_id] ORDER BY [TimestampUTC] ASC),[TimestampUTC] ) as seconds
      ,CASE WHEN a.Username IS NOT NULL AND a.Username != 'NotAvailable' 
		THEN SUBSTRING([Sta_eth_mac], 1, 2) + ':' + SUBSTRING( [Sta_eth_mac], 3, 2) + ':' + SUBSTRING( [Sta_eth_mac], 5, 2) + ':'  + SUBSTRING( [Sta_eth_mac], 7, 2) + ':'  + SUBSTRING( [Sta_eth_mac], 9, 2) + ':' + SUBSTRING( [Sta_eth_mac], 11, 2)  
		WHEN coalesce([Randomized_mac], 0) = 0 THEN 'Ej inloggad'
		ELSE 'Ej inloggad randomized' END AS  [CustomerExternalId]

	  FROM [aruba].[ArubaLocationData] a
	  where CAST([TimestampUTC] AS DATE) = DATEADD(day, -1 , CAST(getdate() AS DATE))
	)x
LEFT OUTER JOIN
[dim].[CustomerDim] C ON
c.CustomerExternalId = x.[CustomerExternalId] and c.[KeyType] = x.[KeyType] and c.Username = x.Username and c.Randomized = x.Randomized_mac
LEFT OUTER JOIN 
[dim].[StoreDim] s ON
s.[StoreExternalId] = x.Building_id
LEFT OUTER JOIN
[dim].[TimeDim] t on
t.Time = CONVERT(VARCHAR(8), x.[Positiontime], 108) 
LEFT OUTER JOIN
[dim].[DateDim] d on 
d.Date = cast(x.[Positiontime] as DATE)
LEFT OUTER JOIN
[dim].[SourceSystemDim] ss on
ss.[Provider_name] = coalesce(x.[Provider_name], 'IPONLY') and ss.[Manufacturer] = 'Aruba'
LEFT OUTER JOIN
[dim].[DeviceTypeDim] dt on
dt.Manufacturer = x.[Device_family] and dt.Model = x.[Device_type] and 
dt.Language is NULL and dt.ModelYear IS NULL and dt.OS IS NULL and dt.OSVersion IS NULL
LEFT OUTER JOIN 
[dim].[ControllerDeviceDim] cd
ON x.Ap_mac = [MACAddress] AND x.Ap_name = cd.Name
LEFT OUTER JOIN
[dim].[FloorDim] f
ON x.Floor_id = f.FloorExternalId and x.Building_id = f.StoreExternalId