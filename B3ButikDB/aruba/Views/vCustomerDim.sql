﻿

/****** Script for SelectTopNRows command from SSMS  ******/
CREATE view [aruba].[vCustomerDim] AS
select * from ( 
SELECT DISTINCT
	  CASE WHEN [Sta_eth_mac] = 'Anonymized' 
		  THEN [Hashed_sta_eth_mac] 
		  ELSE SUBSTRING([Sta_eth_mac], 1, 2) + ':' + SUBSTRING( [Sta_eth_mac], 3, 2) + ':' + SUBSTRING( [Sta_eth_mac], 5, 2) + ':'  + SUBSTRING( [Sta_eth_mac], 7, 2) + ':'  + SUBSTRING( [Sta_eth_mac], 9, 2) + ':' + SUBSTRING( [Sta_eth_mac], 11, 2) 
		  END AS  [CustomerExternalId]
	  ,'MacAdress' AS [KeyType]
	  ,case when [Sta_eth_mac] = 'Anonymized' THEN 1 ELSE 0 END as [Hashed]
	  ,0 AS [Randomized] -- Inloggade har inte randomized
      ,NULL AS  [CustomerInfo]
      ,case WHEN CHARINDEX('@', [Username]) > 0 THEN [Username] ELSE NULL END AS [Email]
      ,[Username]
  FROM [aruba].[ArubaLocationData]
where CAST([TimestampUTC] AS DATE) <= DATEADD(day, -1 , CAST(getdate() AS DATE)) and 
[Username] IS NOT NULL AND [Username] != 'NotAvailable'
)X
WHERE not exists (SELECT * from dim.CustomerDim c where c.CustomerExternalId = x.CustomerExternalId)