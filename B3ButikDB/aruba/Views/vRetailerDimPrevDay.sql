﻿CREATE VIEW aruba.vRetailerDimPrevDay AS
SELECT DISTINCT 
	[Campus_id] AS [RetailerExternalId]  
    ,'N/A' AS [Name] 
    ,'N/A' AS [StreetAdress]
    ,'N/A' AS [Zipcode]
    ,'N/A' AS [City]
    ,'N/A' AS [PhoneNumber]
    ,'N/A' AS [Email]
FROM
[aruba].[ArubaLocationData] A
where NOT EXISTS (SELECT [RetailerExternalId] from [dim].[RetailerDim] R WHERE R.[RetailerExternalId] = A.Campus_id)
AND CAST([TimestampUTC] AS DATE) <= DATEADD(day, -1 , CAST(getdate() AS DATE))