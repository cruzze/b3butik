﻿
create view aruba.vDeviceTypeDim as
select DISTINCT 
a.Device_family AS [Manufacturer]
,a.Device_type AS [Model] 
,NULL AS [ModelYear]
,NULL AS [Language]
,NULL AS [OS]
,NULL AS [OSVersion]
from [aruba].[ArubaLocationData] a
where not exists (SELECT * FROM [dim].[DeviceTypeDim] d where d.Manufacturer = a.Device_family and d.Model = a.Device_type)
and a.Device_family IS NOT NULL AND CAST([TimestampUTC] AS DATE) = DATEADD(day, -1 , CAST(getdate() AS DATE))