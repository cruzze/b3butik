﻿CREATE VIEW aruba.vStoreDimPrevDay AS
SELECT DISTINCT 
	[Building_id] AS [StoreExternalId]  
    ,'N/A' AS [Name] 
    ,'N/A' AS [StreetAdress]
    ,'N/A' AS [Zipcode]
    ,'N/A' AS [City]
    ,'N/A' AS [Region]
    ,'N/A' AS [Country]
    ,0 AS [Longitude]
    ,0 AS [Latitude]
    ,R.RetailerDim_SK AS RetailerDim_SK
    ,NULL AS [CorporateExternaldentity]
    ,NULL AS [Email]
    ,NULL AS [PhoneNumber]
    ,NULL AS [LongitudeList]
    ,NULL AS [LatitudeList]
FROM
[aruba].[ArubaLocationData] A
inner join [dim].[RetailerDim] R
ON R.RetailerExternalId = a.Campus_id
where NOT EXISTS (SELECT [StoreExternalId] from [dim].[StoreDim] S WHERE S.[StoreExternalId] = A.[Building_id])
AND CAST([TimestampUTC] AS DATE) <= DATEADD(day, -1 , CAST(getdate() AS DATE))