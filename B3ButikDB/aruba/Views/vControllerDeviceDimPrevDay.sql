﻿
CREATE VIEW aruba.vControllerDeviceDimPrevDay AS
select DISTINCT 
NULL AS [IPAddres]
,a.Ap_mac AS [MACAddress]
,Ap_name AS [Name],-1 AS [Longitude]
,-1 AS [Latitude]
,DATEADD(DAY, -47, CAST(GETDATE() AS DATE)) AS [StartDate]
,'9999-12-31' AS [EndDate]
,-1[RSSILimitStorePresence]
 from [aruba].[ArubaLocationData] a
 where a.Ap_name IS NOT NULL AND a.Ap_name != 'NotAvailable'
 AND not exists (select * from [dim].[ControllerDeviceDim] d where d.MACAddress = a.Ap_mac and d.[Name] = a.Ap_name)
 AND CAST([TimestampUTC] AS DATE) = DATEADD(day, -1 , CAST(getdate() AS DATE))