﻿CREATE TABLE [aruba].[ArubaLocationData] (
    [Seq]                   BIGINT           NOT NULL,
    [TimestampUTC]          DATETIME2 (7)    NOT NULL,
    [op]                    VARCHAR (50)     NULL,
    [Topic_seq]             BIGINT           NULL,
    [Source_id]             VARCHAR (255)    NULL,
    [Topic]                 VARCHAR (50)     NULL,
    [Hashed_sta_eth_mac]    VARCHAR (255)    NOT NULL,
    [Sta_location_x]        DECIMAL (18, 15) NOT NULL,
    [Sta_location_y]        DECIMAL (18, 15) NOT NULL,
    [Error_level]           INT              NULL,
    [Campus_id]             VARCHAR (255)    NOT NULL,
    [Building_id]           VARCHAR (255)    NOT NULL,
    [Floor_id]              VARCHAR (255)    NOT NULL,
    [Sta_eth_mac]           VARCHAR (50)     NOT NULL,
    [Geofence_ids]          VARCHAR (255)    NULL,
    [Rssi_val]              INT              NOT NULL,
    [Loc_algorithm]         VARCHAR (50)     NOT NULL,
    [Longitude]             DECIMAL (18, 15) NOT NULL,
    [Latitude]              DECIMAL (18, 15) NOT NULL,
    [Altitude]              DECIMAL (18, 15) NOT NULL,
    [Unit]                  VARCHAR (50)     NOT NULL,
    [EventProcessedUtcTime] DATETIME2 (7)    NOT NULL,
    [PartitionId]           INT              NOT NULL,
    [EventEnqueuedUtcTime]  DATETIME2 (7)    NOT NULL,
    [Associated]            VARCHAR (10)     NULL,
    [Device_category]       VARCHAR (255)    NULL,
    [Device_family]         VARCHAR (255)    NULL,
    [Device_type]           VARCHAR (255)    NULL,
    [Username]              VARCHAR (50)     NULL,
    [Sta_ip_address]        VARCHAR (50)     NULL,
    [Sta_ip_af]             VARCHAR (20)     NULL,
    [Hashed_sta_ip_address] VARCHAR (50)     NULL,
    [Role]                  VARCHAR (50)     NULL,
    [Ht]                    VARCHAR (255)    NULL,
    [Ap_name]               VARCHAR (50)     NULL,
    [Ap_mac]                VARCHAR (50)     NULL,
    [Bssid]                 VARCHAR (20)     NULL,
    [Randomized_mac]        CHAR (1)         NULL,
    [Provider_name]         VARCHAR (100)    NULL
);




GO
CREATE NONCLUSTERED INDEX [IX_ArubaLocationData]
    ON [aruba].[ArubaLocationData]([TimestampUTC] ASC);

